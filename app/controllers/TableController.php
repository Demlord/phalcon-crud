<?php
declare(strict_types=1);

class TableController extends Phalcon\Mvc\Controller
{
    // проверка активной сессии
    public function initialize()
    {
        if (!$this->session->has('IS_LOGIN')) {
            return $this->response->redirect('index');
        }
    }

    // получение всех продаж
    public function indexAction()
    {
        $this->tag->setTitle('Список продаж');
        
        try {
            $this->view->data = Sale::find(array(
                'order' => 'date DESC'
            ));
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
    }

    // получение данных о покупателе/консультанте
    public function infoAction()
    {
        // тип: покупатель/консультант
        $type = $this->dispatcher->getParams()[0];
        // идентификатор продажи
        $idSale = $this->dispatcher->getParams()[1];

        try {
            $info = Sale::findFirst("idSale='" . $idSale . "'");

            switch ($type) {
                case 'buyer':
                {
                    $this->tag->setTitle('Сведения о покупателе');

                    // заполнение сведений о покупателе
                    $user = new Buyer();
                    $user->name = $info->buyer->name;
                    $user->surname = $info->buyer->surname;
                    $user->patronymic = $info->buyer->patronymic;
                    $user->phone = $info->buyer->phone;
                    $user->city = $info->buyer->city;
                    $user->street = $info->buyer->street;
                    $user->house = $info->buyer->house;
                    $user->flat = $info->buyer->flat;
                    $user->passportSeries = $info->buyer->passportSeries;
                    $user->passportNumber = $info->buyer->passportNumber;
                } break;
                
                case 'consultant':
                {
                    $this->tag->setTitle('Сведения о консультанте');

                    // заполнение сведений о консултанте
                    $user = new Consultant();
                    $user->name = $info->consultant->name;
                    $user->surname = $info->consultant->surname;
                    $user->patronymic = $info->consultant->patronymic;
                    $user->phone = $info->consultant->phone;
                    $user->city = $info->consultant->city;
                    $user->street = $info->consultant->street;
                    $user->house = $info->consultant->house;
                    $user->flat = $info->consultant->flat;
                    $user->standing = $info->consultant->standing;
                    $user->dateOfBirth = $info->consultant->dateOfBirth;
                } break;
            }

            return $this->view->user = $user;
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
    }

    // добавление записи в БД
    public function addAction($tableName)
    {
        if (!$this->request->isPost()) {
            return $this->response->redirect('index');
        }

        try {
            switch ($tableName) {
                // добавление в таблицу car
                case 'car':
                {
                    $car = new Car();
                    $car->brand = $this->request->getPost("brand");
                    $car->model = $this->request->getPost("model");
                    $result = $car->save();
                } break;
                
                // добавление в таблицу buyer
                case 'buyer':
                {
                    $buyer = new Buyer();
                    $buyer->name = $this->request->getPost("name");
                    $buyer->surname = $this->request->getPost("surname");
                    $buyer->patronymic = $this->request->getPost("patronymic");
                    $buyer->passportSeries = $this->request->getPost("passportSeries");
                    $buyer->passportNumber = $this->request->getPost("passportNumber");
                    $buyer->city = $this->request->getPost("city");
                    $buyer->street = $this->request->getPost("street");
                    $buyer->house = $this->request->getPost("house");
                    $buyer->flat = $this->request->getPost("flat");
                    $buyer->phone = $this->request->getPost("phone");
                    $result = $buyer->save();
                } break;

                // добавление в таблицу sale
                case 'sale':
                {
                    $sale = new Sale();
                    $sale->idBuyer = $this->request->getPost("buyer");
                    $sale->idCar = $this->request->getPost("car");
                    $sale->idConsultant = $this->request->getPost("consultant");
                    $sale->date = $this->request->getPost("date");
                    $sale->colour = $this->request->getPost("colour");
                    $sale->price = $this->request->getPost("price");
                    $result = $sale->save();
                } break;
            }

            if ($result) {
                $this->flash->success("Данные успешно добавлены");
            } else {
                $this->flash->error("Ошибка при добавлении данных");
            }

            return $this->response->redirect('index');
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
    }

    // удаление продаж
    public function deleteAction()
    {
        // идентификаторы удаляемых продаж
        $deleteId = $this->dispatcher->getParams();

        $error = 0;
        $succes = 0;

        foreach ($deleteId as $item) {
            try {
                $result = Sale::findFirst("idSale='" . $item . "'")->delete();
                if ($result) {
                    $succes++;
                }
            } catch (Exception $e) {
                $error++;
            }
        }
        
        if ($succes > 0) {
            $this->flash->success("Удалено записей: " . $succes);
        }
        if ($error > 0) {
            $this->flash->error("Ошибок при удалении записей: " . $error);
        }

        return $this->response->redirect('index');
    }

    // редактирование данных в таблицах
    public function editAction()
    {
        // название таблицы
        $table = $this->dispatcher->getParams()[0];
        // идентификатор записи
        $id = $this->dispatcher->getParams()[1];

        try {
            $sale = Sale::findFirst("idSale='" . $id . "'");

            switch ($table) {
                case 'sale':
                {
                    $sale->idConsultant = $this->request->getPost("consultant");
                    $sale->idBuyer = $this->request->getPost("buyer");
                    $sale->idCar = $this->request->getPost("car");
                    $sale->colour = $this->request->getPost("colour");
                    $sale->price = $this->request->getPost("price");
                    $sale->date = $this->request->getPost("date");
                    $result = $sale->save();
                    $path = "index";
                } break;
                
                case 'buyer':
                {
                    $buyer = $sale->buyer;
                    $buyer->name = $this->request->getPost("name");
                    $buyer->surname = $this->request->getPost("surname");
                    $buyer->patronymic = $this->request->getPost("patronymic");
                    $buyer->passportSeries = $this->request->getPost("passportSeries");
                    $buyer->passportSumber = $this->request->getPost("passportNumber");
                    $buyer->city = $this->request->getPost("city");
                    $buyer->street = $this->request->getPost("street");
                    $buyer->house = $this->request->getPost("house");
                    $buyer->flat = $this->request->getPost("flat");
                    $buyer->phone = $this->request->getPost("phone");
                    $result = $buyer->save();
                    $path = "table/info/buyer/" . $id;
                } break;

                case 'consultant':
                {
                    $consultant = $sale->consultant;
                    $consultant->name = $this->request->getPost("name");
                    $consultant->surname = $this->request->getPost("surname");
                    $consultant->patronymic = $this->request->getPost("patronymic");
                    $consultant->dateOfBirth = $this->request->getPost("dateOfBirth");
                    $consultant->standing = $this->request->getPost("standing");
                    $consultant->city = $this->request->getPost("city");
                    $consultant->street = $this->request->getPost("street");
                    $consultant->house = $this->request->getPost("house");
                    $consultant->flat = $this->request->getPost("flat");
                    $consultant->phone = $this->request->getPost("phone");
                    $result = $consultant->save();
                    $path = "table/info/consultant/" . $id;
                } break;
            }
            
            if ($result) {
                $this->flash->success('Данные успешно изменены');
            } else {
                $this->flash->error('В процессе изменения данных возникла ошибка');
            }

            return $this->response->redirect($path);
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
    }

    // выход из учетной записи
    public function logoutAction()
    {
        // закрытие текущей сессии
        $this->session->destroy();
        return $this->response->redirect('index');
    }
}
