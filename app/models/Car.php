<?php

class Car extends \Phalcon\Mvc\Model
{
    public $idCar;
    public $brand;
    public $model;

    // связь один ко многим с таблицей Sale
    public function initialize()
    {
        $this->hasMany("idCar", "Sale", "idCar");
    }
}
