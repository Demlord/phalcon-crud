<?php

class Consultant extends \Phalcon\Mvc\Model
{
    public $idConsultant;
    public $name;
    public $surname;
    public $patronymic;
    public $standing;
    public $city;
    public $street;
    public $house;
    public $flat;
    public $phone;
    public $password;
    public $dateOfBirth;

    // связь один ко многим с таблицей Sale
    public function initialize()
    {
        $this->hasMany("idConsultant", "Sale", "idConsultant");
    }
}
