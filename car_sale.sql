﻿--
-- Скрипт сгенерирован Devart dbForge Studio 2020 for MySQL, Версия 9.0.304.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 26.07.2020 18:23:16
-- Версия сервера: 5.7.26
-- Версия клиента: 4.1
--

-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS car_sale;

CREATE DATABASE IF NOT EXISTS car_sale
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Установка базы данных по умолчанию
--
USE car_sale;

--
-- Создать таблицу `consultant`
--
CREATE TABLE IF NOT EXISTS consultant (
  idConsultant tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  surname varchar(50) NOT NULL,
  patronymic varchar(50) NOT NULL,
  standing tinyint(2) UNSIGNED NOT NULL,
  city varchar(50) NOT NULL,
  street varchar(50) NOT NULL,
  house smallint(3) UNSIGNED NOT NULL,
  flat smallint(3) UNSIGNED NOT NULL,
  phone bigint(11) UNSIGNED NOT NULL,
  password varchar(100) NOT NULL,
  dateOfBirth date NOT NULL,
  PRIMARY KEY (idConsultant)
)
ENGINE = INNODB,
AUTO_INCREMENT = 5,
AVG_ROW_LENGTH = 4096,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `car`
--
CREATE TABLE IF NOT EXISTS car (
  idCar smallint(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  brand varchar(25) NOT NULL,
  model varchar(35) NOT NULL,
  PRIMARY KEY (idCar)
)
ENGINE = INNODB,
AUTO_INCREMENT = 6,
AVG_ROW_LENGTH = 4096,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `buyer`
--
CREATE TABLE IF NOT EXISTS buyer (
  idBuyer smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  surname varchar(50) NOT NULL,
  patronymic varchar(50) NOT NULL,
  passportSeries smallint(4) UNSIGNED NOT NULL,
  passportNumber mediumint(6) UNSIGNED NOT NULL,
  city varchar(50) NOT NULL,
  street varchar(50) NOT NULL,
  house smallint(3) UNSIGNED NOT NULL,
  flat smallint(3) UNSIGNED NOT NULL,
  phone bigint(11) UNSIGNED NOT NULL,
  PRIMARY KEY (idBuyer)
)
ENGINE = INNODB,
AUTO_INCREMENT = 4,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать таблицу `sale`
--
CREATE TABLE IF NOT EXISTS sale (
  idSale mediumint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  idBuyer smallint(6) UNSIGNED NOT NULL,
  date date NOT NULL,
  idCar smallint(6) UNSIGNED NOT NULL,
  colour varchar(25) NOT NULL,
  price int(8) UNSIGNED NOT NULL,
  idConsultant tinyint(3) UNSIGNED NOT NULL,
  PRIMARY KEY (idSale)
)
ENGINE = INNODB,
AUTO_INCREMENT = 39,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Создать внешний ключ
--
ALTER TABLE sale
ADD CONSTRAINT FK_sale_buyer_idBuyer FOREIGN KEY (idBuyer)
REFERENCES buyer (idBuyer) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sale
ADD CONSTRAINT FK_sale_car_idCar FOREIGN KEY (idCar)
REFERENCES car (idCar) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE sale
ADD CONSTRAINT FK_sale_consultant_id_consultant FOREIGN KEY (idConsultant)
REFERENCES consultant (idConsultant);

-- 
-- Вывод данных для таблицы consultant
--
INSERT INTO consultant VALUES
(1, 'Иван', 'Иванов', 'Иванович', 3, 'Волжский', 'Свердлова', 17, 58, 89612348961, '91091ac32f525d88daa6d6b721420ac1', '1985-07-13'),
(2, 'Петр', 'Петров', 'Петрович', 3, 'Волжский', 'Карбышева', 29, 140, 89024379512, '576a43faa848c65a59e47f3a533a9fe1', '1992-03-28'),
(3, 'Николай', 'Власов', 'Викторович', 1, 'Волгоград', 'Мира', 13, 156, 88005553535, '790d653b5a66d5df7c67ec576e9e8f9a', '1993-11-08'),
(4, 'Василий', 'Шуйский', 'Егорович', 4, 'Волжский', 'Дружбы', 11, 6, 89277113456, '92251e8665e19be62c86ff039528e16e', '1979-06-17');

-- 
-- Вывод данных для таблицы car
--
INSERT INTO car VALUES
(1, 'Лада', '2107'),
(2, 'BMW', 'M3 E46'),
(3, 'Mercedes', 'G63'),
(4, 'Subaru', 'WRX STI'),
(5, 'Toyota', 'Camry');

-- 
-- Вывод данных для таблицы buyer
--
INSERT INTO buyer VALUES
(1, 'Василий', 'Ломакин', 'Викторович', 1111, 123456, 'Волгоград', 'Мира', 1, 1, 88005553535),
(2, 'Георгий', 'Свиридов', 'Александрович', 5471, 675140, 'Москва', 'Новый Арбат', 45, 74, 89624836495),
(3, 'Дмитрий', 'Нагиев', 'Владимирович', 1414, 895730, 'Смоленск', 'Ленина', 6, 144, 84951759283);

-- 
-- Вывод данных для таблицы sale
--
INSERT INTO sale VALUES
(33, 2, '2020-07-09', 1, 'Черный', 455000, 1),
(36, 2, '2020-07-16', 3, 'Черный', 10000000, 3);

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
--
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;