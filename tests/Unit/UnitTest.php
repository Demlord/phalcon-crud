<?php

declare(strict_types=1);
namespace Tests\Unit;

use IndexController;

class UnitTest extends AbstractUnitTest
{
    public function testTestCase(): void
    {
    	$testVar = new IndexController();

        $this->assertEquals
        (
            $testVar->SUM_unitTest(15, 10), 25
        );
    }
}